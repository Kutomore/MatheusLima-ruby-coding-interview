require 'rails_helper'

RSpec.describe "Users", type: :request do

  RSpec.shared_context 'with multiple companies' do
    let!(:company_1) { create(:company) }
    let!(:company_2) { create(:company) }

    before do
      5.times do
        create(:user, company: company_1)
      end
      5.times do
        create(:user, company: company_2)
      end
    end
  end

  RSpec.shared_context 'with user' do
    let!(:company) { create(:company) }

    before do
      create(:user, company: company, username: 'Match')
      create(:user, company: company, username: 'Miss')
    end
  end

  describe "#index" do
    let(:result) { JSON.parse(response.body) }

    context 'when fetching users by company' do
      include_context 'with multiple companies'

      it 'returns only the users for the specified company' do
        get company_users_path(company_1)
        
        expect(result.size).to eq(company_1.users.size)
        expect(result.map { |element| element['id'] } ).to eq(company_1.users.ids)
      end
    end

    context 'when fetching users by username' do
      include_context 'with user'

      context 'with a suffix for the name' do
        it 'returns only the users which match the specified name' do
          get users_path, params: { username: 'tch' }

          expect(result.size).to eq(1)
          expect(result.map { |element| element['username'] } ).to eq(['Match'])
        end
      end

      context 'with a prefix for the name' do
        it 'returns only the users which match the specified name' do
          get users_path, params: { username: 'Ma' }

          expect(result.size).to eq(1)
          expect(result.map { |element| element['username'] } ).to eq(['Match'])
        end
      end

      context 'with a full match for the name' do
        it 'returns only the users which match the specified name' do
          get users_path, params: { username: 'Match' }

          expect(result.size).to eq(1)
          expect(result.map { |element| element['username'] } ).to eq(['Match'])
        end
      end

      context 'with no matches for the name' do
        it 'returns an empty array' do
          get users_path, params: { username: 'Non-existing' }

          expect(result.size).to eq(0)
        end
      end

      context 'with no name sent' do
        it 'returns all the users' do
          get users_path, params: { username: '' }

          expect(result.size).to eq(2)
          expect(result.map { |element| element['username'] } ).to match_array(%w[Match Miss])
        end
      end
    end

    context 'when fetching all users' do
      include_context 'with multiple companies'

      it 'returns all the users' do

      end
    end
  end
end
